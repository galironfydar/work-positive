<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::any('/', ['as' => 'index', 'before' => 'results', 'uses' => 'IndexController@index']);

/**
 * Ensures the user has completed the first step before accessing later routes
 */
Route::group(['before' => 'session'], function () {

    Route::any('/{context}/cards', ['as' => 'cards', 'uses' => 'CardController@cards']);
    Route::any('/{context}/sort', ['as' => 'sort', 'uses' => 'CardController@sort']);
    Route::any('/images', ['as' => 'images', 'uses' => 'ImageController@images']);
    Route::any('/notes', ['as' => 'notes','before' =>'sanitize','uses' => 'ImageController@notes']);
    Route::get('/questionnaire', ['as' => 'questionnaire', 'uses' => 'QuestionnaireController@questionnaire']);
    Route::post('/questionnaire', ['as' => 'questionnaire', 'before' => 'csrf | sanitize ', 'uses' => 'QuestionnaireController@questionnaire']);

});

Route::any('/thanks', function() {
    Session::flush();
    return View::make('thanks');
});

Route::any('/results/{id}',		['as' => 'results', 'uses' => 'ResultController@results']);
Route::any('/conference',		['as' => 'conference', 'uses' => 'ConferenceController@results']);