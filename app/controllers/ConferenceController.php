<?php

class ConferenceController extends BaseController
{


    public function results()
    {
        $contextsArray = ['contexts' => array()];

        if ($this->isPostRequest()) {

            if(Input::has('filters')) {

                $filters = Input::get('filters');

                $results = DB::table('result_context_cards')
                    ->join('cards', 'result_context_cards.card_id', '=', 'cards.id')
                    ->join('result_contexts', 'result_contexts.id', '=', 'result_context_cards.result_context_id')
                    ->join('results', 'results.id', '=', 'result_contexts.result_id')
                    ->join('users', 'users.id', '=', 'results.user_id')
                    ->where(function ($query) use ($filters) {
                        foreach ($filters as $key => $data) {
                            if ($data !== '') {
                                if ($key !== "interests") {
                                    $query->where("users.$key", '=', $data);
                                }
                            }
                        }
                    })->where('users.conference', 'yes')->where('users.research', 'yes')
                    ->select(DB::raw('count(*) as count, cards.title'))->orderBy('count',
                        'DESC')->groupBy('cards.title')->get();

                $users = DB::table('users')->selectRaw("DISTINCT users.id")
                    ->where(function ($query) use ($filters) {
                        foreach ($filters as $key => $data) {
                            if ($data !== '') {
                                if ($key !== "interests") {
                                    $query->where("users.$key", '=', $data);
                                }
                            }
                        }
                    })->where('users.conference', 'yes')->where('users.research', 'yes')
                    ->get();

                $num = sizeof($users);

                if (isset($filters['interests'])) {

                    $users = DB::table('users')->selectRaw("DISTINCT id")
                        ->where(function ($query) use ($filters) {
                            foreach ($filters as $key => $data) {
                                if ($data !== '') {
                                    if ($key !== "interests") {
                                        $query->where("users.$key", '=', $data);
                                    }
                                }
                            }
                        })->where('users.conference', 'yes')->where('users.research', 'yes')
                        ->lists('id');

                    $ids = [];

                    foreach ($users as $index => $id) {

                        $check = DB::table('user_interests')
                            ->where('user_id', $id)
                            ->where(function ($query) use ($filters) {
                                foreach ($filters['interests'] as $interest) {
                                    $query->orWhere('interest', $interest);
                                }
                            })->count();

                        if ($check == sizeof($filters['interests'])) {
                            array_push($ids, $id);
                        }

                    }

                    $results = DB::table('result_context_cards')
                        ->join('cards', 'result_context_cards.card_id', '=', 'cards.id')
                        ->join('result_contexts', 'result_contexts.id', '=', 'result_context_cards.result_context_id')
                        ->join('results', 'results.id', '=', 'result_contexts.result_id')
                        ->join('users', 'users.id', '=', 'results.user_id')
                        ->whereIn('users.id', $ids)
                        ->select(DB::raw('count(*) as count, cards.title'))->orderBy('count',
                            'DESC')->groupBy('cards.title')->get();

                    $num = sizeof($ids);

                }

            } else {

                $results = DB::table('result_context_cards')
                    ->join('cards', 'result_context_cards.card_id', '=', 'cards.id')
                    ->join('result_contexts', 'result_contexts.id', '=', 'result_context_cards.result_context_id')
                    ->join('results', 'results.id', '=', 'result_contexts.result_id')
                    ->join('users', 'users.id', '=', 'results.user_id')
                    ->where('users.conference', 'yes')->where('users.research', 'yes')
                    ->select(DB::raw('count(*) as count, cards.title'))->orderBy('count',
                        'DESC')->groupBy('cards.title')->get();

                $users = DB::table('users')->selectRaw("DISTINCT users.id")
                    ->where('users.conference', 'yes')->where('users.research', 'yes')
                    ->get();

                $num = sizeof($users);

            }

            $data = [
                'results' => $results,
                'users' => $num
            ];

            return Response::json($data);

        } else {

            $results = DB::table('result_context_cards')
                ->join('cards', 'result_context_cards.card_id', '=', 'cards.id')
                ->join('result_contexts', 'result_contexts.id', '=', 'result_context_cards.result_context_id')
                ->join('results', 'results.id', '=', 'result_contexts.result_id')
                ->join('users', 'users.id', '=', 'results.user_id')
                ->where('users.conference', 'yes')->where('users.research', 'yes')
                ->select(DB::raw('count(*) as count, cards.title'))->orderBy('count', 'DESC')->groupBy('cards.title')->get();

            $users = DB::table('users')
                ->where('conference', 'yes')->where('research', 'yes')->count();

            return View::make('conference')
                ->with('results', json_encode($results))
                ->with('users', json_encode($users))
                ->with('cards', json_encode(Card::lists('title')));

        }


    }

    /**
     * Checks whether the current request is a POST request
     * @return bool
     */
    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }

}


