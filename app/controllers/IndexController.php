<?php

class IndexController extends BaseController {

    public function index()
    {
        if(!$this->isPostRequest()) {

            return View::make("index")
                ->with("contexts", Context::all());

        } else {

            $contexts = [];

            /**
             * Takes the chosen contexts and adds them to session
             */
            foreach(Input::get("contexts") as $id) {
                $context = Context::find($id);
                $contexts[strtolower($context->title)] = $context->toArray();
            }

            Session::put("contexts", $contexts);

            return Redirect::route("cards", ["context" => key($contexts)]);

        }
    }

    public function back()
    {
        $previous = explode('/', URL::previous());
        $previous = end($previous);

        // Reverses the session context array to get most recently changed context first
        $contexts = array_reverse(Session::get("contexts"));

        switch ($previous) {
            case 'cards':
                /**
                 * Checks if a context has already had strengths chosen and sorted
                 * If it has it removes the sorted array and redirects back to the sorting page.
                 */
                foreach($contexts as $key => &$context) {
                    if(isset($context["sorted"])) {
                        unset($context["sorted"]);
                        Session::set("contexts", array_reverse($contexts));
                        return Redirect::route('sort');
                    }
                }
                /**
                 * If no contexts have been completed it removes all chosen contexts from session
                 * and redirects back to the index page
                 */
                Session::forget("contexts");
                return Redirect::route("index");

            case 'sort':
                /**
                 * Takes the last context to have strengths chosen and removes the strengths array
                 * Redirects the user back to strength selection for that context.
                 */
                foreach($contexts as $key => &$context) {
                    if(isset($context["strengths"])) {
                        unset($context["strengths"]);
                        Session::set("contexts", array_reverse($contexts));
                        return Redirect::route("cards");
                    }
                }

            case 'images':
                /**
                 * Takes the last sorted context and removes the sorted array
                 * Redirects the user back to the sorting page for that context.
                 */
                foreach($contexts as $key => &$context) {
                    if(isset($context["sorted"])) {
                        unset($context["sorted"]);
                        Session::set("contexts", array_reverse($contexts));
                        return Redirect::route("sort");
                    }
                }

            case 'notes':
                /**
                 * Removes the chosen image from session and redirects back to the image
                 * selection page
                 */
                Session::forget("image");
                return Redirect::route("images");
        }

    }

    /**
     * Checks whether the current request is a POST request
     * @return bool
     */
    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }

}


