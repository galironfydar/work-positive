<?php

class ContextsController extends BaseController
{

    public function contexts()
    {
        if (!$this->isPostRequest()) {


            return View::make("contexts")
                ->with('sData', Session::all());


        } else {


        }
    }


    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }
}


