<?php

class QuestionnaireController extends BaseController
{

    public function questionnaire()
    {
        if (!$this->isPostRequest()) {

            /**
             * Returns the questionnaire page
             */
            return View::make("questionnaire");

        } else {
            /**
             * Creates the user in the database if user does not already exist
             */
            if(User::where('email', '=', Input::get('email'))->count() > 0) {

                $user = User::where('email', '=', Input::get('email'))->first();


            } else {

                $user = User::create([
                    "email"     => Input::get('email'),
                    "subscribe" => Input::get('subscribe'),
                    "gender"    => Input::get('gender'),
                    "continent" => Input::get('continent'),
                    "country"   => Input::get('country'),
                    "conference" => Input::get('conf'),
                    "firstname" => Input::get('firstname'),
                    "lastname" => Input::get('lastname'),
                    "research" => Input::get('research')
                ]);

            }

            /**
             * Adds interest relationships to user
             */
            foreach(Input::get('interest') as $interest) {

                if($interest != null) {
                    Interest::create([
                        'interest' => $interest,
                        'user_id' => $user->id
                    ]);
                }
            }

            /**
             * Creates the users results set
             */

            $randStr = $this->generateRandomString(16);

            $result = Result::create([
                "user_id"    => $user->id,
                "image_id"   => Session::get("image")["id"],
                "image_note" => Session::get("image")["note"],
                "url"        => $randStr,
            ]);

            /**
             * Attaches the contexts and associated cards to the results set
             */
            foreach (Session::get("contexts") as $context) {
                $resultContext = new ResultContext;
                $resultContext->context_id = $context['id'];
                $resultContext->result_id = $result->id;
                $resultContext->score = Input::get($context['title']);
                $resultContext->save();
                foreach ($context["sorted"] as $key => $sorted) {
                    foreach ($sorted as $index => $value) {
                        $resultContextCard = new ResultContextCard;
                        $resultContextCard->card_id = $value;
                        $resultContextCard->priority = $key;
                        $resultContextCard->order = $index;
                        $resultContextCard->resultContext()->associate($resultContext);
                        $resultContextCard->save();
                    }
                }
            }

            Session::set("results", $result->id);

            Mail::send('emails.results', ['url' => $result->url, 'user' => $user], function($message) use ($user)
            {
                $message->to($user->email, $user->email)
                    ->from('feedback@atmybest.com')
                    ->subject('Your strengths summary from At my best');
            });

            /**
             * Redirects the user to the results page
             */
            return Redirect::to("/thanks");

        }
    }


    /**
     * Checks whether the current request is a POST request
     * @return bool
     */
    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }

    protected function generateRandomString($length = 16)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
           $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}




