<?php

class ImageController extends BaseController {

    public function images()
    {
        if(!$this->isPostRequest()) {

            /**
             * Returns the image selection page
             */
            return View::make("images")
                ->with("cards", Card::all())
                ->with("contexts", Session::get("contexts"));

        } else {

            /**
             * Adds chosen image to session
             */
            Session::put("image.id", Input::get("id"));

            return Redirect::route("notes");

        }
    }


    public function notes()
    {
        if(!$this->isPostRequest()) {

            /**
             * Retrieves the chosen image from session for display on the page
             */
            $id = Session::get("image")["id"];
            $card = Card::find($id);

            return View::make("notes")
                ->with("card", $card);

        } else {

            /**
             * Adds note to chosen session image
             */
            Session::put("image.note", Input::get('note'));

            return Redirect::route("questionnaire");

        }
    }

    /**
     * Checks whether the current request is a POST request
     * @return bool
     */
    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }
}


