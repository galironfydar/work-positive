<?php

class ResultController extends BaseController
{
    public function results($id)
    {
        $contextsArray = ['contexts' => array()];

        $result = Result::where('url', '=', $id)->first();

        foreach ($result->getContext as $context) {

            $high = array();
            $medium = array();
            $low = array();

            $title = $context->context->title;

            foreach ($context->cards as $card) {
                switch ($card->priority) {
                    case 'low':
                        array_push($low, $card->card->title);
                        break;
                    case 'med':
                        array_push($medium, $card->card->title);
                        break;
                    case 'high':
                        array_push($high, $card->card->title);
                        break;
                }
            }

            array_push($contextsArray['contexts'],
                [
                    'name' => $title,
                    'strengths' =>
                        [
                            'high' => $high,
                            'medium' => $medium,
                            'low' => $low
                        ]
                ]
            );

        }

        return View::make('results')
            ->with('result', $result)
            ->with('contexts', $this->organiseContextCards($result))
            ->with('data', json_encode($contextsArray));

    }


    protected function organiseContextCards(Result $result)
    {
        $context1 = [];
        $context2 = [];

        $res = [];

        foreach ($result->get_context as $r) {

            $query = ResultContextCard::select("result_context_cards.card_id");
            $query->where('result_context_cards.result_context_id', '=', $r->id);
            $query->orderByRaw("
                    case when priority = 'high' then 1
                          when priority = 'med' then 2
                          when priority = 'low' then 3
                     end
                    ");
            $query->orderBy("order", "ASC");

            array_push($res, $query->lists('card_id'));

        }

        $result1 = $res[0];
        $result2 = $res[1];

        return [
            "{$result->getContext->first()->context->title}" => $result1,
            "{$result->getContext->last()->context->title}" => $result2
        ];

    }

    /**
     * Checks whether the current request is a POST request
     * @return bool
     */
    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }

}


