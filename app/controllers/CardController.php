<?php

class CardController extends BaseController {

    public function cards($page_context)
    {
        /**
         * Grab the current context being used
         */
        $context = Session::get("contexts")[$page_context];

        if (!$this->isPostRequest()) {

            if(Session::has("contexts.$page_context.sorted")) {
                Session::forget("contexts.$page_context.sorted");
            }

            /**
             * Return the strength selection page
             */
            return View::make("cards")
                ->with("step", (array_search($page_context, array_keys(Session::get("contexts"))) == 0) ? 2 : 4)
                ->with("context", $context)
                ->with("cards", Card::all());

        } else {

            /**
             * Stores the chosen strengths against the context in session
             */
            Session::put("contexts.$page_context.strengths", Input::get("strengths"));

            return Redirect::route("sort", ["context" => $page_context]);

        }
    }

    public function sort($page_context)
    {
        /**
         * Grab the current context being used
         */
        $context = Session::get("contexts")[$page_context];

        if (!$this->isPostRequest()) {

            /**
             * Grab the strengths chosen for the context
             */
            $cards = Card::whereIn("id", $context["strengths"])->get();

            /**
             * Return the sorting page with chosen strengths
             */
            return View::make("sort")
                ->with("step", (array_search($page_context, array_keys(Session::get("contexts"))) == 0) ? 3 : 5)
                ->with("context", $context)
                ->with("page_context", $page_context)
                ->with("cards", $cards);

        } else {

            /**
             * Adds the sorted strengths to the context in session
             */
            Session::put("contexts.$page_context.sorted", Input::get("strengths"));

            $contexts = Session::get("contexts");

            /**
             * Works out the next context to have strengths chosen from session
             * and returns the URL to route to
             */
            while(key($contexts) !== null && key($contexts) == $page_context) {
                next($contexts);
                Session::flash("next_context", key($contexts));
                return Response::json([
                    "status" => "success",
                    "url" => URL::route("cards", ["context" => key($contexts)])
                ]);
            }

            /**
             * If all contexts in session have had strengths chosen and sorted
             * the images page URL is sent back
             */
            return Response::json([
                "status" => "success",
                "url" => URL::route("images")
            ]);

        }
    }

    /**
     * Checks whether the current request is a POST request
     * @return bool
     */
    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }

}


