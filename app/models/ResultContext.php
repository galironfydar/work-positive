<?php

class ResultContext extends Eloquent {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'result_contexts';


    /**
     * Sets the model to check for timestamp columns
     * @var bool
     */
    public $timestamps = true;



    /**
     * The attributes protected from mass assignment
     * @var array
     */
    protected $guarded = ['id'];


    protected $with = ['context', 'cards'];


    public function result()
    {
        return $this->belongsTo('Result');
    }


    public function context()
    {
        return $this->belongsTo('Context');
    }


    public function cards()
    {
        return $this->hasMany('ResultContextCard');
    }

}
