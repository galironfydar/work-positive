<?php

class Result extends Eloquent {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'results';


    /**
     * Sets the model to check for timestamp columns
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes protected from mass assignment
     * @var array
     */
    protected $guarded = ['id'];


    protected $with = ['getContext'];


    public function user()
    {
        return $this->belongsTo('User');
    }


    public function image()
    {
        return $this->belongsTo('Card', 'image_id');
    }


    public function getContext()
    {
        return $this->hasMany('ResultContext');
    }


    public function getCards()
    {
        return $this->hasManyThrough('ResultContextCard', 'ResultContext', 'result_id', 'result_context_id');
    }

}
