<?php

class ResultContextCard extends Eloquent {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'result_context_cards';


    /**
     * Sets the model to check for timestamp columns
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes protected from mass assignment
     * @var array
     */
    protected $guarded = ['id'];


    protected $with = ['card'];


    public function resultContext()
    {
        return $this->belongsTo('ResultContext');
    }


    public function card()
    {
        return $this->belongsTo('Card');
    }

}
