<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';


	/**
	 * Sets the model to check for timestamp columns
	 * @var bool
	 */
	public $timestamps = true;


	protected $hidden = array('password', 'remember_token');


	/**
	 * Validation rules
	 * @var array
	 */
	static $rules = [
		'email' => 'required|email',
	];


	/**
	 * The attributes protected from mass assignment
	 * @var array
	 */
	protected $guarded = ['id'];


	/**
	 * The relationships to be loaded whenever a user is grabbed
	 * @var array
	 */
	protected $with = ['result'];


	public function result()
	{
		return $this->hasMany('Result');
	}

	public function interests()
	{
		return $this->hasMany('Interest', 'user_id', 'id');
	}



}
