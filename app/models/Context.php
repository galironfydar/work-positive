<?php

class Context extends Eloquent {


    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'contexts';


    /**
     * Sets the model to not check for timestamp columns
     * @var bool
     */
    public $timestamps = false;


    /**
     * Validation rules
     * @var array
     */
    static $rules = [
        'title' => 'max:32'
    ];


    /**
     * The attributes protected from mass assignment
     * @var array
     */
    protected $guarded = ['id'];


    public function getResultContext()
    {
        return $this->hasMany('ResultContext');
    }


}
