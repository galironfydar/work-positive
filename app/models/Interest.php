<?php

class Interest extends Eloquent {


    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'user_interests';


    /**
     * Sets the model to not check for timestamp columns
     * @var bool
     */
    public $timestamps = false;


    /**
     * Validation rules
     * @var array
     */
    static $rules = [
        'interest' => 'required'
    ];


    /**
     * The attributes protected from mass assignment
     * @var array
     */
    protected $guarded = ['id'];


    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}
