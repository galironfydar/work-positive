<?php

class ContextTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("contexts")->delete();

        Context::create(["title" => "Work"]);
        Context::create(["title" => "School"]);
        Context::create(["title" => "Home"]);
        Context::create(["title" => "Community"]);
    }

}
