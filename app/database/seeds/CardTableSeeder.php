<?php

class CardTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("cards")->delete();

        Card::create(["image" => "1.jpg", "title" => "Supportive"]);
        Card::create(["image" => "2.jpg", "title" => "Organised"]);
        Card::create(["image" => "3.jpg", "title" => "Thoughtful"]);
        Card::create(["image" => "4.jpg", "title" => "Independent"]);
        Card::create(["image" => "5.jpg", "title" => "Determined"]);
        Card::create(["image" => "6.jpg", "title" => "Approachable"]);
        Card::create(["image" => "7.jpg", "title" => "Flexible"]);
        Card::create(["image" => "8.jpg", "title" => "Open-minded"]);
        Card::create(["image" => "9.jpg", "title" => "Insightful"]);
        Card::create(["image" => "10.jpg", "title" => "Playful"]);
        Card::create(["image" => "11.jpg", "title" => "Analytical"]);
        Card::create(["image" => "12.jpg", "title" => "Passionate"]);
        Card::create(["image" => "13.jpg", "title" => "Curious"]);
        Card::create(["image" => "14.jpg", "title" => "Decisive"]);
        Card::create(["image" => "15.jpg", "title" => "Resilient"]);
        Card::create(["image" => "16.jpg", "title" => "Practical"]);
        Card::create(["image" => "17.jpg", "title" => "Conscientious"]);
        Card::create(["image" => "18.jpg", "title" => "Brave"]);
        Card::create(["image" => "19.jpg", "title" => "Spontaneous"]);
        Card::create(["image" => "20.jpg", "title" => "Creative"]);
        Card::create(["image" => "21.jpg", "title" => "Positive"]);
        Card::create(["image" => "22.jpg", "title" => "Calm"]);
        Card::create(["image" => "23.jpg", "title" => "Sincere"]);
        Card::create(["image" => "24.jpg", "title" => "Appreciative"]);
        Card::create(["image" => "25.jpg", "title" => "Rigorous"]);
        Card::create(["image" => "26.jpg", "title" => "Wise"]);
        Card::create(["image" => "27.jpg", "title" => "Fair"]);
        Card::create(["image" => "28.jpg", "title" => "Co-operative"]);
        Card::create(["image" => "29.jpg", "title" => "Detailed"]);
        Card::create(["image" => "30.jpg", "title" => "Hopeful"]);
        Card::create(["image" => "31.jpg", "title" => "Energetic"]);
        Card::create(["image" => "32.jpg", "title" => "Considered"]);
        Card::create(["image" => "33.jpg", "title" => "Warm"]);
        Card::create(["image" => "34.jpg", "title" => "Attentive"]);
        Card::create(["image" => "35.jpg", "title" => "Strategic"]);
        Card::create(["image" => "36.jpg", "title" => "Caring"]);
        Card::create(["image" => "37.jpg", "title" => "Patient"]);
        Card::create(["image" => "38.jpg", "title" => "Understanding"]);
        Card::create(["image" => "39.jpg", "title" => "Modest"]);
        Card::create(["image" => "40.jpg", "title" => "Engaging"]);
        Card::create(["image" => "41.jpg", "title" => "Focused"]);
        Card::create(["image" => "42.jpg", "title" => "Empathetic"]);
        Card::create(["image" => "43.jpg", "title" => "Dependable"]);
        Card::create(["image" => "44.jpg", "title" => "Principled"]);
        Card::create(["image" => "45.jpg", "title" => "Guiding"]);
        Card::create(["image" => "46.jpg", "title" => "Generous"]);
        Card::create(["image" => "47.jpg", "title" => "Adventurous"]);
        Card::create(["image" => "48.jpg", "title" => "Humorous"]);
    }

}
