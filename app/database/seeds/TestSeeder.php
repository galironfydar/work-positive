<?php

class TestSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $people = 50;
        $start = 0;

        $faker = Faker\Factory::create();

        for($u = $start; $u< $people; $u++){
            User::create(['email'=> time() . $faker->email,
            'conference'=>'yes',
            'subscribe'=>'yes',
            'research'=>'yes']);
        };

        for($r = $start; $r< $people; $r++){
            Result::create(['user_id'=>$faker->numberBetween($min = $start, $max = $people),
                'image_id'=>4,
                'image_note'=>"TEESSSSTT"]);
        };

        for($rc = $start; $rc< $people; $rc++){
            $resultId=$faker->numberBetween($min = $start, $max = $people);
            for($k = 0; $k<2; $k++){
                ResultContext::create(['context_id'=>$faker->numberBetween($min = 1, $max = 4),
                    'score'=>8,
                    'result_id'=>$resultId]);
            }

        };

        for($cc = $start; $cc< $people; $cc++){
            $cardnum = $faker->numberBetween($min = 5, $max = 12);
            for($s = 0; $s<$cardnum;$s++){
                ResultContextCard::create([
                    'card_id'=>$faker->numberBetween($min = 1, $max =48),
                    'priority'=>'med',
                    'order'=>$s,
                    'result_context_id'=>$faker->numberBetween($min = $start, $max = $people * 2)
                    ]);

            };
        };

    }

}
