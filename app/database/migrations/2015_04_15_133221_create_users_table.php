<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('email', 50)->unique();
			$table->string('firstname', 250)->nullable();
			$table->string('lastname', 250)->nullable();
			$table->string('gender', 12)->nullable();
			$table->string('continent', 100)->nullable();
			$table->string('country', 100)->nullable();
			$table->string('subscribe')->nullable();
			$table->string('conference', 12)->nullable();
			$table->string('research', 12)->nullable();

			$table->timestamps();
		});

		Schema::create('user_interests', function(Blueprint $table) {
			$table->increments('id');
			$table->string('interest');

			$table->integer('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('user_interests');
	}

}
