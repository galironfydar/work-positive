<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('results', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('image_id')->references('id')->on('cards');
			$table->text('image_note');
			$table->string('url', 32);
			$table->timestamps();
		});

		Schema::create('result_contexts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('score');
			$table->integer('result_id')->references('id')->on('results');
			$table->integer('context_id')->references('id')->on('contexts');
			$table->timestamps();
		});

		Schema::create('result_context_cards', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('card_id')->references('id')->on('cards');
			$table->string('priority', 12);
			$table->integer('order');
			$table->integer('result_context_id')->references('id')->on('results_contexts');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *	 * @return void
	 */
	public function down()
	{
		Schema::drop('results');
		Schema::drop('result_contexts');
		Schema::drop('result_context_cards');
	}

}


