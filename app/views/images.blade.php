@extends("templates.master")

@section("header")

    @include("templates.header")

@stop

@section("content")

    <div class="container">

        {{ Form::open(['class' => 'image-form']) }}

        <h2>
            Select a picture that captures the story of you <span class="orange">at your best.</span>
        </h2>

        <div class="image-info row">

            <div class="col-sm-4 col-sm-offset-2">
                <a class="info collapsed" data-toggle="collapse" href="#how" aria-expanded="false" aria-controls="how">
                    How do I choose?
                </a>
                <div class="row top15"></div>

            </div>

            <div class="col-sm-4">
                <a class="info collapsed" data-toggle="collapse" href="#cards" aria-expanded="false" aria-controls="cards"
                   aria-controls="comparing">
                    Remember my chosen strengths
                </a>
                <div class="row top15"></div>

            </div>

            <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">

                @include("info.images.how")

                @include("info.images.cards")

            </div>

        </div>

        <!-- Display Images-->
        <div class="row card-row">
            @foreach($cards as $card)
                <div class="col-md-2 col-sm-4">
                    @include("partials.image")
                </div>
            @endforeach
        </div>

        {{ Form::hidden("id") }}

    </div>

    <!-- Navigation -->
    <section class="fixed-elements">

    <div id="sticky-nav" class="row">

        <div class="container">

            <div class="col-sm-2">
                <button type="button" class="back-btn btn btn-block btn-orange" onclick="goBack();">Back</button>
            </div>

            <div class="col-sm-10">
                <div class="progress-holder">
                    <span class="circle complete">1</span>
                    <span class="circle complete">2</span>
                    <span class="circle complete">3</span>
                    <span class="circle complete">4</span>
                    <span class="circle complete">5</span>
                    <span class="circle active">6</span>
                    <span class="circle">7</span>
                </div>
            </div>

        </div>
    </div>

    </section>

    {{ Form::close() }}

@stop

@section("scripts")

    <script type="text/javascript">
        $(document).ready(function() {
            $('#flashModal').modal();
            centerModals();
        });

        function centerModals(){
            $('.modal').each(function(i){
                var $clone = $(this).clone().css('display', 'block').appendTo('body');
                var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
                top = top > 0 ? top : 0;
                $clone.remove();
                $(this).find('.modal-content').css("margin-top", top);
            });
        }


    </script>
    <div class="modal fade" id="flashModal" tabindex="-1" role="dialog" aria-labelledby="flashModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close orange" data-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <p class="lead">
                        Brilliant! You've now chosen the strengths you use
                        <?php $count = 1; ?>
                        @foreach($contexts as $title => $context)
                            {{ ($title !== 'community') ? 'at ' . $title : 'in your ' . $title }}
                            @if($count < sizeof($contexts))
                                and
                            @else
                                .
                            @endif
                            <?php $count++; ?>
                        @endforeach
                        There's just one more thing to do - choose a picture that tells the story of you at your best.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default modal-next-context" data-dismiss="modal">Next</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/js/vendor/ekko/ekko-lightbox.js"></script>
    <script src="{{ asset("js/image-selection.js") }}"></script>

@stop