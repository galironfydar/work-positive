@extends("templates.master")

@section("header")

    @include("templates.header")

@stop

@section("content")

    <div class="container">

        <div class="row">

            <p>Woo hoo! You've finished making your selections and in just a minute you'll be able to reflect on your strengths in more detail.</p>

            <h2>A bit more about <span class="orange">you</span></h2>

        </div>

        {{ Form::open(['class' => 'questionnaire-form']) }}

        <div class="row container questions">

            <div class="row">
                <div class="col-sm-12 muted">
                    <p>To access your strengths summary, just add your contact details below and we’ll send your personal link straight away.</p>
                </div>
            </div>

            <section id="meta" class="row">

                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('name', 'What is your name?', ['class' => 'orange']) }}
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6">

                            {{Form::text('firstname', null, ['placeholder' => 'First Name', 'class' => 'col-md-12 col-sm-12'])}}

                        </div>

                        <div class="col-sm-6">

                            {{Form::text('lastname', null, ['placeholder' => 'Last Name', 'class' => 'col-md-12 col-sm-12'])}}

                        </div>

                    </div>

                </div>

            </section>

            <section id="mail" class="row">

                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('email', 'Your email*', ['class' => 'orange'])}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::email('email', false, ['class' => 'col-sm-12 col-md-6', 'required' => 'required']) }}
                        </div>
                    </div>

                </div>

            </section>

            <div class="row">
                <div class="col-sm-12">
                    <p>Reflector Solo is a brand new tool and you’re one of the very first people to try it out. We’d love it if you would help us out with our research by answering just a few more questions. Don’t worry, your responses will be anonymised.</p>
                </div>
            </div>

            <section id="sex" class="row">

                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('gender', 'What is your gender?', ['class' => 'orange']) }}
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="radio">
                                {{ Form::radio('gender', 'male', false, ['id' => 'male'])}}
                                {{ Form::label('male', 'Male', ['class' => 'lbl-align-center']) }}
                            </div>

                            <div class="radio">
                                {{ Form::radio('gender', 'female', false, ['id' => 'female']) }}
                                {{ Form::label('female', 'Female', ['class' => 'lbl-align-center']) }}
                            </div>

                            <div class="radio">
                                {{ Form::radio('gender', 'gnotsay', false , ['id' => 'gnotsay']) }}
                                {{ Form::label('gnotsay', 'Rather not say', ['class' => 'lbl-align-center']) }}
                            </div>

                        </div>

                    </div>

                </div>

            </section>

            <section id="nation" class="row">

                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('nationality', 'What is your nationality?', ['class' => 'orange']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            {{ Form::select('continent', ['default' => 'Please select your continent'], 'default', ['id' => 'continentlist', 'class' => 'col-md-12 col-sm-12']) }}
                        </div>
                        <div class="col-sm-6">
                            {{ Form::select('country', ['default' => 'Please select your country'], 'default', ['id' => 'countrylist', 'class' => 'col-md-12 col-sm-12']) }}
                        </div>
                    </div>

                </div>

            </section>

            <section id="int" class="row">

                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('interest', 'What are your main areas of interest?', ['class' => 'orange']) }}
                        </div>
                    </div>

                    <div class="row">

                        <div id="int1" class="col-sm-12">

                            <div class="checkbox">
                                {{ Form::checkbox('interest[]', 'Work & Organisations', false, ['id' => 'workandorg']) }}
                                {{ Form::label('workandorg', 'Work & Organisations', ['class' => 'lbl-align-center']) }}
                            </div>

                            <div class="checkbox">
                                {{ Form::checkbox('interest[]', 'Education', false, ['id' => 'education']) }}
                                {{ Form::label('education', 'Education', ['class' => 'lbl-align-center']) }}
                            </div>

                            <div class="checkbox">
                                {{ Form::checkbox('interest[]', 'Clinical', false, ['id' => 'clinical']) }}
                                {{ Form::label('clinical', 'Clinical', ['class' => 'lbl-align-center']) }}
                            </div>

                            <div class="checkbox">
                                {{ Form::checkbox('interest[]', 'Health', false, ['id' => 'health']) }}
                                {{ Form::label('health', 'Health', ['class' => 'lbl-align-center']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::text('interest[]', false, ['id' => 'other', 'class' => 'col-sm-12', 'placeholder' => 'Other']) }}
                            </div>

                        </div>

                    </div>

                </div>

            </section>

            <?php $count = 1; ?>
            @foreach(Session::get("contexts") as $index => $context)

                <section id="context{{ $context['id'] }}" class="row">

                    <div class="container col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::hidden($context['title'], null, ['id' => 'slider'.$count.'amount', 'class' => 'slideramont', 'readonly' => 'readonly']) }}
                                @if($context['title'] == 'Community')
                                    {{ Form::label($context['title'], 'How much of the time are you flourishing in your ' .  strtolower($context['title']) . '?', ['class' => 'orange']) }}
                                @else
                                    {{ Form::label($context['title'], 'How much of the time are you flourishing at ' .  strtolower($context['title']) . '?', ['class' => 'orange']) }}
                                @endif
                            </div>
                        </div>

                        <div class="slider row">
                            <div class="col-sm-12">
                                <div id="slider{{ $count }}"></div>
                                <div class="pull-left">None of the time</div>
                                <div class="pull-right">All of the time</div>
                            </div>
                        </div>
                    </div>

                </section>

                <?php $count++; ?>

            @endforeach

            <section id="conf" class="row">

                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('conf', 'Did you attend the IPPA World Congress in Orlando (25-28 June)? *  ', ['class' => 'orange']) }}
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="radio">
                                {{ Form::radio('conf', 'yes', false, ['id' => 'conf_yes'])}}
                                {{ Form::label('conf_yes', 'Yes', ['class' => 'lbl-align-center']) }}
                            </div>

                            <div class="radio">
                                {{ Form::radio('conf', 'no', false, ['id' => 'conf_no']) }}
                                {{ Form::label('conf_no', 'No', ['class' => 'lbl-align-center']) }}
                            </div>

                        </div>
                    </div>

                </div>

            </section>

            <section id="research" class="row">

                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('research', 'Can we anonymously include your responses in our research? * ', ['class' => 'orange']) }}
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="radio">
                                {{ Form::radio('research', 'yes', false, ['id' => 'res_yes'])}}
                                {{ Form::label('res_yes', 'Yes', ['class' => 'lbl-align-center']) }}
                            </div>

                            <div class="radio">
                                {{ Form::radio('research', 'no', false, ['id' => 'res_no']) }}
                                {{ Form::label('res_no', 'No', ['class' => 'lbl-align-center']) }}
                            </div>

                        </div>

                    </div>

                </div>

            </section>

        </div>
    </div>

    <!-- Navigation -->
    <div id="sticky-nav" class="" class="row">

        <div class="container">

            <div class="col-sm-2">
                <button type="button" class="back-btn btn btn-block btn-orange" onclick="goBack();">Back</button>
            </div>

            <div class="col-sm-8">
                
            </div>

            <div class="col-sm-2">
                {{ Form::submit('Next', ['class' => 'btn btn-block btn-orange']) }}
            </div>
        </div>

    </div>

    {{ Form::close() }}

@stop

@section("scripts")

    <script src="{{ asset("js/questionnaire.js") }}"></script>

@stop