@extends("templates.master")

@section("header")

    @include("templates.header")

@stop

@section("content")

    <div class="container">

        {{ Form::open() }}

        <h6>Please select <span class="orange">5 to 12</span> cards that reflect your strengths
            <span class="orange">{{ ($context['title'] !== 'Community') ? 'at ' . strtolower($context['title']) : 'in your ' . strtolower($context['title']) }}</span>.
        </h6>

        <div class="row">

            <div class="col-sm-4 col-sm-offset-2">
                <a class="info collapsed" data-toggle="collapse" href="#why" aria-expanded="false" aria-controls="why">
                    Why this number?
                </a>
                <div class="row top15"></div>

            </div>

            <div class="col-sm-4">
                <a class="info collapsed" data-toggle="collapse" href="#how" aria-expanded="false" aria-controls="how">
                    How do I choose the right words?
                </a>
                <div class="row top15"></div>

            </div>

            <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">

                @include("info.cards.why")

                @include("info.cards.how")

            </div>

        </div>

        {{ Form::hidden('context', $context["id"]) }}

        <!-- Cards Display -->
        <div class="row card-row">
            @foreach($cards as $card)
                <div class="col-lg-2 col-md-3 col-sm-3">
                    @include("partials.card")
                </div>
            @endforeach
        </div>
    </div>



    <section class="fixed-elements">
    <div class="shortlist text-center row">

        <p class="counter">
            <img src="{{ asset('img/logo_white.png') }}" alt="At my best logo"/>
            There are 48 cards for you to browse. You have selected <span class="orange"><span class="count">0</span> cards</span>
        </p>

    </div>

    <!-- Navigation -->
    <div id="sticky-nav" class="row">

        <div class="container">
            <div class="col-sm-2">
                <button type="button" class="back-btn btn btn-block btn-orange" onclick="goBack();">Back</button>
            </div>

            <div class="col-sm-8">
                <div class="progress-holder">
                    <span class="circle complete">1</span>
                    <span class="circle {{ ($step == 2) ? "active" : "complete" }}">2</span>
                    <span class="circle {{ ($step > 2) ? "complete" : false }}">3</span>
                    <span class="circle {{ ($step == 4) ? "active" : false }}">4</span>
                    <span class="circle">5</span>
                    <span class="circle">6</span>
                    <span class="circle">7</span>
                </div>
            </div>

            <div class="col-sm-2">
                {{ Form::submit('Next', ['class' => 'next-btn btn btn-block btn-orange', 'disabled' => true]) }}
            </div>
        </div>


    </div>
    </section>

    {{ Form::close() }}

@stop

@section("scripts")


    <!-- First Context Modal-->
    @if(Session::get("next_context") == '')

        <script type="text/javascript">
            $(document).ready(function() {
                console.log('1');
                $('#flashModal0').modal();
                centerModals();
            });

            function centerModals(){
                $('.modal').each(function(i){
                    var $clone = $(this).clone().css('display', 'block').appendTo('body');
                    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
                    top = top > 0 ? top : 0;
                    $clone.remove();
                    $(this).find('.modal-content').css("margin-top", top);
                });
            }
        </script>
        <div class="modal fade" id="flashModal0" tabindex="-1" role="dialog" aria-labelledby="flashModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close orange" data-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <p class="lead">
                        First, which strengths do you show
                            <span class="orange">{{ ($context['title'] !== 'Community') ? 'at ' . strtolower($context['title'])  : 'in your ' . strtolower($context['title'])}}</span>
                            ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-next-context" data-dismiss="modal">Next</button>
                    </div>
                </div>
            </div>
        </div>


    @endif



    <!-- Second Context Modal-->
    @if(Session::has("next_context"))
        <script type="text/javascript">
            $(document).ready(function() {
                $('#flashModal').modal();
                centerModals();
            });

            function centerModals(){
                $('.modal').each(function(i){
                    var $clone = $(this).clone().css('display', 'block').appendTo('body');
                    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
                    top = top > 0 ? top : 0;
                    $clone.remove();
                    $(this).find('.modal-content').css("margin-top", top);
                });
            }
        </script>
        <div class="modal fade" id="flashModal" tabindex="-1" role="dialog" aria-labelledby="flashModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close orange" data-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <p class="lead">
                            Great stuff! Let's move on to think about {{ (Session::get("next_context") !== 'community') ? 'your strengths ' : 'the strengths you show ' }}
                            <span class="orange">{{ (Session::get("next_context") !== 'community') ? 'at ' . Session::get("next_context") : 'in your ' . Session::get("next_context") }}</span>.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-next-context" data-dismiss="modal">Next</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <script>
        var min = 5,
                max = 12;
    </script>
    <script src="{{ asset("js/card-selection.js") }}"></script>

@stop