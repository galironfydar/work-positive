<p>Hi {{{ $user->firstname }}} </p>

<p>It’s brilliant that you’ve taken the time to complete Reflector&trade; Solo, the new strengths tool from At my best&trade;. We
    hope you found it a useful way to reflect on what you do best.</p>

<p>Follow the link below and you’ll be whisked away to your very own strengths summary page, where you can reflect
    further on your strengths and how you use them in different parts of your life.</p>

<a href="{{ URL::to('/results/' . $url) }}"><p>Yes please, take me there right now!</p></a>

<p>
    Reflector&trade; Solo is a brand new tool from At my best&trade;...
    As a thank you for testing it out we would like to offer you 50% OFF the price of the At my best™ strengths cards (which Reflector is based upon).
    Just use the discount code REFLECTOR when you buy single packs of cards from <a href="http://shop.atmybest.com">www.shop.atmybest.com</a> (valid until 31st August 2015).
</p>

<p>
    If you think Reflector&trade; could help you in your work - e.g. as a coach, facilitator, teacher, counsellor or people manager - we’d be thrilled to hear from you!
    Likewise, if you have any feedback or suggestions for how we can improve it then please get in touch.
</p>

<p>
    By the way, we’re already working on Reflector&trade; 360 – a 360-degree feedback tool which will reveal what strengths your friends, family and colleagues value most in you!
</p>

<p>
    We’ll keep you updated with very occasional emails but please let us know if you’d rather we don’t darken your inbox again!
</p>

<p>
    Be your best self.
</p>

<p>
    The At my best&trade; team.
</p>

<a href="http://www.atmybest.com">www.atmybest.com</a>
