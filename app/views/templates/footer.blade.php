<footer role="navigation" class="l-footer navbar navbar-default navbar-static-bottom">

    <div class="col-sm-2">

        <span class="pull-left">
        <a href="//atmybest.com/" target="_blank">
            <img class="logo logo-footer" src="{{ asset("img/logo_h_grey.png") }}" alt="At my best"/>
        </a>
        </span>

    </div>

    <div id="web-links" class="col-sm-7 text-center">

        <a class="col-sm-4" href="http://www.atmybest.com/contact" target="_blank">Contact Us</a>
        <a class="col-sm-4" href="http://www.atmybest.com/reflector-privacy-policy" target="_blank">Privacy Policy</a>
        <a class="col-sm-4" href="http://www.atmybest.com/reflector-terms-of-use" target="_blank">Terms Of Use</a>

    </div>

    <div class="col-sm-3">

        <small class="colophon pull-right">&copy; 2014 Work Positive Limited</small>

    </div>


</footer>