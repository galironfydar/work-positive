<!doctype html>
<html class="no-js {{ SiteHelpers::bodyClass() }}" lang="en">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Solo</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href={{ asset('solo-favicon.png') }}>
      <link rel="apple-touch-icon" href="{{ asset('apple-touch-icon.png') }}">

      <link rel="stylesheet" href="{{ asset('css/vendor/bootstrap/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/vendor/flat-ui/flat-ui.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/vendor/ekko/ekko-lightbox.css') }}">
      <link rel="stylesheet" href="{{ asset('css/fonts/icomoon/style.css') }}">
      <link rel="stylesheet" href="{{ asset('css/style.css') }}">
      @yield("styles")

  </head>

  <body>

    <div class="wrap">

        <header role="banner" class="l-header navbar navbar-inverse navbar-static-top">

            @yield("header")

        </header>

        <div class="post-header">
            <div class="tail row">
                <div class="notch col-sm-12"></div>
            </div>
        </div>

        <div class="browser-notice">
            <h2>This website is optimised for larger devices. Please visit on a tablet, laptop or desktop computer</h2>
        </div>

        <main role="main" class="l-content">

            @yield("content")

        </main>

        @include("templates.footer")

    </div>

    <script>
        var BASE_URL = "{{ URL::to("") }}";
    </script>

    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="{{ asset('js/vendor/jquery/jquery.ui.touch-punch.min.js') }}"></script>
    <script src="{{ asset('js/vendor/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-scrolltofixed.js') }}"></script>


    <script src="//use.typekit.net/ice4igl.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>
    <script src="{{ asset('js/main.js') }}"></script>
    @yield("scripts")

  </body>
</html>
