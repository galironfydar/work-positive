<section class="card image">

    <div class="row">
        <img id="{{ $card->id }}" src="{{ asset("img/cards/" . $card->image) }}" alt="{{ $card->title }}" class="img-responsive pick"/>
    </div>

    <div class="row image-choices">

        <a class="enlarge cards" id="{{ $card->id }}" href="{{ asset("img/cards/" . $card->image) }}" data-toggle="lightbox" data-width="940" data-gallery="multiimages" data-id="{{ $card->id }}" data-footer=" " data-title="">
            <i class="glyphicon glyphicon-zoom-in"></i> Enlarge
        </a>

    </div>

</section>