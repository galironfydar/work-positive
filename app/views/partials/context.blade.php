<section class="card context col-lg-2 col-md-3 col-sm-3">
    <input type="checkbox" name="contexts[]" id="{{  "card" . $context->id }}" value="{{ $context->id }}"/>
    <label for="{{  "card" . $context->id }}">
        <p>{{ $context->title }}</p>
    </label>
</section>