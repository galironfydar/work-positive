<section class="card">
    <input type="checkbox" name="strengths[]" id="{{  "card" . $card->id }}" value="{{ $card->id }}"/>
    <label for="{{  "card" . $card->id }}">
        <img src="{{ asset("img/logo.png") }}" alt="{{ $card->title }}"/>
        <p>{{ $card->title }}</p>
    </label>
</section>