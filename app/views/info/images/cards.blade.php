<div class="info-content info-white collapse" id="cards">
    <div class="row">

        @foreach($contexts as $title => $context)
            <div class="col-sm-6 text-center">
                <p class="lead">{{ ucfirst($title) }}</p>
                @foreach($context["strengths"] as $card)
                    <p>{{ Card::find($card)->title }}</p>
                @endforeach
            </div>
        @endforeach

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#cards" aria-expanded="false" aria-controls="cards">Close</a>
        </div>

    </div>
    <br/>
</div>