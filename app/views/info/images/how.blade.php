<div class="info-content info-white collapse" id="how">
    <div class="row">

        <div class="col-sm-12">
            <p>Look through the pictures and think about when you are at your best. You might find a photo that reminds you of a particular moment or one that has an abstract connection with your strength.  The important thing is to choose something that has meaning for you.</p>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#how" aria-expanded="false" aria-controls="how">Close</a>
        </div>

    </div>
    <br/>
</div>