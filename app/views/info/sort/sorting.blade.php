<div class="info-content info-white collapse" id="sorting">
    <div class="row">

        <div class="col-sm-12">
            <p>Simply click on each card and drag it into the appropriate section.  Arrange the cards within the groups by putting the strengths you use most at the top.</p>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#sorting" aria-expanded="false" aria-controls="sorting">Close</a>
        </div>
    </div>
    <br/>
</div>