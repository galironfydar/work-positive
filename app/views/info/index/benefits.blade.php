<div class="info-content info-white collapse" id="benefits">
    <div class="row">

        <div class="col-sm-12">
            <p>It’s a tool based on the At my best&trade; strengths cards that helps you identify your strengths.  Strengths are the personal qualities that come naturally to you, that help you to perform to a high standard and bring out your passion.  In other words, they are ‘you, at your best’.</p>


            <p>Reflector Solo gives you the opportunity to reflect on and compare the strengths you use in two different parts of your life using words and pictures.</p>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#benefits" aria-expanded="false" aria-controls="benefits">Close</a>
        </div>
    </div>
    <br/>
</div>