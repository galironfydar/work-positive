<div class="info-content info-white collapse" id="comparing">
    <div class="row">

        <div class="col-sm-12">
            <p>Most people use different strengths in different parts of their lives.  Reflecting on these differences can help you to make even better use of your strengths; for example thinking about how you can apply the strengths you use at home in a work setting.</p>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#comparing" aria-expanded="false" aria-controls="comparing">Close</a>
        </div>
    </div>
    <br/>
</div>