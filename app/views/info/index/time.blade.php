<div class="info-content info-white collapse" id="time">
    <div class="row">

        <div class="col-sm-12">
            <p>Probably 10 – 15 minutes, may be a bit longer if you want to spend some time reflecting on your answers.</p>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#time" aria-expanded="false" aria-controls="time">Close</a>
        </div>

    </div>
    <br/>
</div>