<div class="info-content info-white collapse" id="cards">
    <div class="row">

        <div class="col-sm-12">
            This is a list of the strengths you chose for each part of your life, with the ones you use most frequently showing at the top.  Some questions you might think about are:
            <ul>
                <li>When you compare these two lists, what do you notice?</li>
                <li>Are the lists true reflections of how you feel you are using your strengths in your life at the moment?</li>
                <li>Which strengths could you be using more?  How? When?</li>
            </ul>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#cards" aria-expanded="false" aria-controls="sorting">Close</a>
        </div>

    </div>
    <br/>
</div>