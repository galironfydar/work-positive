<div class="info-content info-white collapse" id="wordcloud">
    <div class="row">

        <div class="col-sm-12">
            The size of each word reflects how often you said you use that strength across both contexts.  Bigger words represent those strengths you use more often.
            <br><br>
            Looking at the word cloud, ask yourself the following questions:
            <ul>
                <li>Thinking across your life, does this feel about right?  </li>
                <li>Are there any strengths that should be bigger/smaller based on how much you are using your strengths now?</li>
                <li>Are there any strengths you would like to be bigger/smaller based on how much you would like to be using your strengths?  What changes can you make?</li>
                <li>If you asked someone else (a friend, family member, colleague) about your strengths would their word cloud for you look the same as this one?</li>
            </ul>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#wordcloud" aria-expanded="false" aria-controls="sorting">Close</a>
        </div>

    </div>
    <br/>
</div>