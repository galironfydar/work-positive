<div class="info-content info-white collapse" id="venndiagram">
    <div class="row">

        <div class="col-sm-12">
            Reflecting on how you use your strengths across different parts of your life can be helpful.  Using the Venn diagram ask yourself:
            <br>
            <ul>
                <li>Is there a notable difference between the number of strengths in each part of the Venn diagram?  How do you feel about that?  What would you change?</li>
                <li>Taking each context in turn, which of the strengths listed could you be using more in the other part of your life?  What impact would that have?</li>
                <li>If people who know you from each part of your life were to choose your strengths, what would the Venn diagram look like? Would it include the same or different words? </li>
            </ul>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#venndiagram" aria-expanded="false" aria-controls="sorting">Close</a>
        </div>

    </div>
    <br/>
</div>