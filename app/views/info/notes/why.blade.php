<div class="info-content info-white collapse" id="why">
    <div class="row">

        <div class="col-sm-12">
            <p>You know the phrase ‘a picture is worth a thousand words’.  Many people find using pictures as prompts helps them to build a richer story around who they are at their best.  What you write in the notes section is up to you.  If the image reminds you of a particular time when you were at your best, tell that story.  If the association is more abstract, perhaps reminding you of how you feel when you are at your best, then note that down.</p>
            <p>You can write as little or as much as you like.  It simply needs to serve as a prompt for you.  Being able to bring the image and story to mind can serve as a useful reminder of how you are when you are at your best.</p>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#why" aria-expanded="false" aria-controls="why">Close</a>
        </div>

    </div>
    <br/>
</div>