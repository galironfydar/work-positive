<div class="info-content info-white collapse" id="how">
    <div class="row">

        <div class="col-sm-12">
            <p>Think of the moments when you have been at your best in this context.  Which words best describe the personal qualities you called upon?  These strengths are not defined - just choose the words that seem most appropriate to you.</p>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#how" aria-expanded="false" aria-controls="how">Close</a>
        </div>

    </div>
    <br/>
</div>