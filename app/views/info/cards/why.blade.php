<div class="info-content info-white collapse" id="why">
    <div class="row">

        <div class="col-sm-12">
            <p>You can't count your strengths like you can count fingers, there is no set or ideal number.  It's not about how many you've got, it's about how you use them!  But for most people between 5-12 strengths is a realistic number to identify and work with.  Remember, you are focusing on those things that really are you at your best, those qualities that energize you and help you excel.</p>
        </div>

        <div class="col-sm-12">
            <a class="close-info" data-toggle="collapse" href="#why" aria-expanded="false" aria-controls="why">Close</a>
        </div>

    </div>
    <br/>
</div>