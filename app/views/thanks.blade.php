@extends("templates.master")

@section("header")

    @include("templates.header")

@stop

@section("content")

    <div class="container">

        <div class="row">

            <h1>You're a <span class="orange">star</span>!</h1>

            <p>You’ll shortly receive an email containing a link to your personal strengths page, which will help you reflect more on your strengths.</p>
            <br/>
            <p>Did you know that you can buy your own set of At my best™ Strengths Cards <a class="orange" href="http://shop.atmybest.com/collections/cards" target="_blank">here</a> or get a print of the <a class="orange" href="http://shop.atmybest.com/collections/photo-prints" target="_blank">photo</a> that reflects you at your best?  We deliver internationally too.</p>
            <br/>
        </div>

        <p></p>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <img src="{{asset('img/amb-cards.png')}}"/>
            </div>
            <div class="col-sm-2"></div>
        </div>

        <div class="row">
            <br/>
            <br/>
            <p>Thanks for being one of the very first people to complete Reflector&trade; Solo!  We’d love to know what you think, so please drop us an <a class="orange" href="mailto:feedback@atmybest.com" target="_blank">email</a> with your feedback.</p>
            <br/><br/>
        </div>

    </div>

@stop