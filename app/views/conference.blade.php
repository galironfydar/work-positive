@extends("templates.conferenceMaster")

@section("content")
    <div class="holder" id="conference-page">
        <img class="toplogo" src="{{ asset('/img/logo_white.png') }}" alt="logo"/>

        <div class="cloud-holder">
            <div id="cloud"></div>
        </div>

        <section class="formContainer">

            <div class="container">

                <form class="filter-form">

                    <section id="nationality" class="row">

                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::label('nationality', 'Filter By Nationality', ['class' => 'orange']) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                {{ Form::select('continent', ['' => 'Select a continent'], '', ['id' => 'continentlist', 'class' => 'col-md-12 col-sm-12']) }}
                            </div>
                            <div class="col-sm-6">
                                {{ Form::select('country', ['' => 'Select a country'], '', ['id' => 'countrylist', 'class' => 'col-md-12 col-sm-12']) }}
                            </div>
                        </div>

                    </section>

                    <section id="gender" class="row">
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::label('gender', 'Filter By Gender', ['class' => 'orange']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-2">
                                {{ Form::radio('gender', 'male', false, ['id' => 'male'])}}
                                {{ Form::label('male', 'Male', ['class' => 'lbl-align-center']) }}
                            </div>
                            <div class="col-sm-2">
                                {{ Form::radio('gender', 'female', false, ['id' => 'female']) }}
                                {{ Form::label('female', 'Female', ['class' => 'lbl-align-center']) }}
                            </div>
                            <div class="col-sm-6"></div>
                            <div class="col-sm-2">
                                <p>Users: <span id="users"></span> </p>
                            </div>
                        </div>
                    </section>

                </form>

                <form class="interest-form">

                    <section id="interest" class="row">
                        <div class="row">
                            <div class="col-sm-3">
                                {{ Form::label('interest', 'Filter By Interest', ['class' => 'orange']) }}
                            </div>
                        </div>

                        <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::checkbox('interest[]', 'Work & Organisations', false, ['id' => 'workandorg']) }}
                                    {{ Form::label('workandorg', 'Work & Organisations', ['class' => 'lbl-align-center']) }}
                                </div>
                                <div class="col-sm-3">
                                    {{ Form::checkbox('interest[]', 'Education', false, ['id' => 'education']) }}
                                    {{ Form::label('education', 'Education', ['class' => 'lbl-align-center']) }}
                                </div>
                                <div class="col-sm-3">
                                    {{ Form::checkbox('interest[]', 'Clinical', false, ['id' => 'clinical']) }}
                                    {{ Form::label('clinical', 'Clinical', ['class' => 'lbl-align-center']) }}
                                </div>
                                <div class="col-sm-3">
                                    {{ Form::checkbox('interest[]', 'Health', false, ['id' => 'health']) }}
                                    {{ Form::label('health', 'Health', ['class' => 'lbl-align-center']) }}
                                </div>
                        </div>

                    </section>

                </form>

                <div class="row filterButtons">
                        <div class="container">

                            <div class="col-sm-2"></div>
                            <div class="col-sm-3">
                                <button class="btn btn-block btn-orange" onclick="loadData()"> Filter</button>
                                <br>
                            </div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-3">
                                <button class="btn btn-block btn-orange" onclick="resetData()"> Reset </button>
                            </div>
                            <div class="col-sm-2"></div>

                        </div>
                    </div>

            </div>

        </section>

    </div>
@stop

@section("scripts")
    <script src="{{ asset('js/conference.js') }}"></script>
    <script src="{{ asset('js/d3.js')}}"></script>
    <script src="{{ asset('js/wordCloudConference.js')}}"></script>
    <script src="{{ asset('js/d3.layout.cloud.js')}}"></script>
    <script>
        $("conference").submit(function () {
            return false;
        });

        var results = {{ $results }};
        var cards = {{ $cards }};
        var users = {{ $users }};
        var resetData  = function () {

            $('form.filter-form')[0].reset();
            $('form.interest-form')[0].reset();

            $.ajax({
                type: "POST",
                url: "/conference",
                success: function(data) {
                    console.log(data);
                    reDrawConfWordCloud(data.results);
                    updateNum(data.users);
                }
            });
        };
        $(document).ready(function () {
            drawWordConfCloud(results);
            updateNum(users);
        });



    </script>
@stop