@extends("templates.master")

@section("header")

    @include("templates.header")

@stop

@section("content")

    <div class="container">

        {{ Form::open() }}

        <h2>How does this picture tell the <span class="orange">story</span> of your strengths?</h2>

        <div class="image-info row">

            <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">
                <a class="info collapsed" data-toggle="collapse" href="#why" aria-expanded="false"
                   aria-controls="why">
                    Why write notes?
                </a>
                <div class="row top15"></div>

                @include("info.notes.why")
            </div>

        </div>

        <!-- Chosen image and notes display -->
        <div class="row notes-row">

            <div class="col-sm-5">
                <img id="{{ $card->id }}" src="{{ asset("img/cards/" . $card->image) }}" alt="{{ $card->title }}" class="img-responsive"/>
            </div>

            <div class="col-sm-7">
                {{ Form::textarea("note", null, ['class' => '', 'placeholder' => "What's the story"]) }}
            </div>

        </div>
    </div>


    <!-- Navigation -->
    <section class="fixed-elements">

    <div id="sticky-nav"  class="row">

        <div class="container">

            <div class="col-sm-2">
                <button type="button" class="back-btn btn btn-block btn-orange" onclick="goBack();">Back</button>
            </div>

            <div class="col-sm-8">
                <div class="progress-holder">
                    <span class="circle complete">1</span>
                    <span class="circle complete">2</span>
                    <span class="circle complete">3</span>
                    <span class="circle complete">4</span>
                    <span class="circle complete">5</span>
                    <span class="circle complete">6</span>
                    <span class="circle active">7</span>
                </div>
            </div>

            <div class="col-sm-2">
                {{ Form::submit('Next', ['class' => 'next-btn btn btn-block btn-orange', 'disabled' => true]) }}
            </div>
        </div>

    </div>
    </section>

    {{ Form::close() }}

@stop

@section("scripts")

    <script src="{{ asset("js/notes.js") }}"></script>

@stop