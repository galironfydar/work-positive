@extends("templates.master")

@section("header")

    @include("templates.header")

@stop

@section("content")

    <div class="container">

        <h6>
            How much do you use these {{ sizeof($cards) }} strengths <span
                    class="orange">{{ ($context['title'] !== 'Community') ? 'at ' . strtolower($context['title']) : 'in your ' . strtolower($context['title']) }}</span>?
            Sort the cards into the 3 groups.
        </h6>

        <div class="row">
            <div class="col-sm-12 ">
                <a class="info collapsed" data-toggle="collapse" href="#sorting" aria-expanded="false"
                   aria-controls="sorting">
                    Not sure how to sort them?
                </a>
                <div class="row top15"></div>
                <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">
                    @include("info.sort.sorting")
                </div>
            </div>
        </div>

        <!-- Display of cards to be sorted -->
        <div class="row">
            <div class="stack sortable cards col-sm-4">

            </div>
        </div>

        <!-- Sortable card drop areas -->
        <div class="row card-row row-centered">

            {{ Form::hidden('context', $context["id"]) }}

            <div class="col-sm-4 col-lg-3 col-centered">
                <section class="impact impact-1">
                    <p class="impact-title">A little</p>

                    <form id="low" class="droppable">

                        @if(Session::has("contexts.{$page_context}.sorted.low"))
                            @foreach(Session::get("contexts.{$page_context}.sorted.low") as $id)
                                <?php $card = Card::find($id); ?>
                                @include("partials.card-list")
                            @endforeach
                        @endif

                    </form>
                    <div class="clearfix"></div>
                </section>
            </div>

            <div class="col-sm-4 col-lg-3 col-centered">
                <section class="impact impact-2">
                    <p class="impact-title">Some</p>

                    <form id="med" class="droppable">

                        @if(Session::has("contexts.{$page_context}.sorted.med"))
                            @foreach(Session::get("contexts.{$page_context}.sorted.med") as $id)
                                <?php $card = Card::find($id); ?>
                                @include("partials.card-list")
                            @endforeach
                        @endif

                    </form>
                    <div class="clearfix"></div>
                </section>
            </div>

            <div class="col-sm-4 col-lg-3 col-centered">
                <section class="impact impact-3">
                    <p class="impact-title">A lot</p>

                    <form id="high" class="droppable">

                        @if(Session::has("contexts.{$page_context}.sorted.high"))
                            @foreach(Session::get("contexts.{$page_context}.sorted.high") as $id)
                                <?php $card = Card::find($id); ?>
                                @include("partials.card-list")
                            @endforeach
                        @endif

                    </form>
                    <div class="clearfix"></div>
                </section>
            </div>

        </div>

    </div>

    <br/>
    <section class="fixed-elements">

        @if(!Session::has("contexts.{$page_context}.sorted"))

            <div class="shortlist row">

                <h3>Your shortlist</h3>

                <div class="cards sortable">

                        @foreach($cards as $card)
                            @include("partials.card-list")
                        @endforeach

                </div>

            </div>

        @endif

        <!-- Navigation -->
        <div id="sticky-nav" class="row">

            <div class="container">

                <div class="col-sm-2">
                    <button type="button" class="back-btn btn btn-block btn-orange" onclick="goBack();">Back</button>
                </div>

                <div class="col-sm-8">
                    <div class="progress-holder">
                        <span class="circle complete">1</span>
                        <span class="circle complete">2</span>
                        <span class="circle {{ ($step == 3) ? "active" : "complete" }}">3</span>
                        <span class="circle {{ ($step > 3) ? "complete" : false }}">4</span>
                        <span class="circle {{ ($step == 5) ? "active" : false }}">5</span>
                        <span class="circle">6</span>
                        <span class="circle">7</span>
                    </div>
                </div>

                <div class="col-sm-2">
                    {{ Form::submit('Next', ['class' => 'next-btn btn btn-block btn-orange', 'disabled' => true]) }}
                </div>
            </div>

        </div>
    </section>

@stop

@section("scripts")

    <script>
        var cardsSorted = {{ (Session::has("contexts.{$page_context}.sorted")) ? 1 : 0  }}

        var num_cards = {{ sizeof($cards) }},
                context = "{{ strtolower($context["title"]) }}";

    </script>
    <script src="{{ asset("js/card-drag-sort.js") }}"></script>
@stop