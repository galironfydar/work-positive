@extends("templates.master")

@section("header")

    @include("templates.header")

@stop

@section("content")

    <div id="results-slider" class="results-slider">

        <div class="slide" data-slidr="one">

            <h2>Your strengths <span class="orange">reflected</span></h2>

            <div class="row text-left">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="col-sm-12">

                        <p>Hi {{User::find($result->user_id)->firstname}}, welcome to your personal strengths page.</p>

                        <p>Here you can reflect on your strengths in more detail by viewing:</p>
                        <ul>
                            <li>A <span class="orange">Venn diagram</span> that helps you compare the strengths you use
                                in different parts of your
                                life
                            </li>
                            <li>A <span class="orange">side-by-side list</span> showing how much you use your strengths
                                in each context
                            </li>
                            <li>Your own <span class="orange">strengths word cloud</span>, giving you a visual reminder
                                of you at your best
                            </li>
                        </ul>


                        <p>Each page includes ‘top tips’ to help you make the most of the information. </p>


                        <p id="atmybestinfo">Don’t forget you can get packs of the physical At my best™ Strengths Cards
                            from our <span class="orange"><a href="http://shop.atmybest.com/" target="_blank">shop</a></span>.
                            They’re really popular with coaches, facilitators, teachers and people managers and are now
                            being used in more than 40 countries!</p>

                        <div id="shop" class="row">
                            <p class="col-sm-4"></p>
                            <a id="shopbtn" class="col-sm-4 text-center" href="http://shop.atmybest.com/"
                               target="_blank">Go to our shop</a>

                            <p class="col-sm-4"></p>
                        </div>


                        <div id="get-in-touch" class="row">
                            <h2 class="text-center">Want to get in <span class="orange">touch?</span></h2>
                        </div>


                        <p id="social-links" class="text-center col-sm-12">

                            <a class="col-sm-6 col-xs-12" href="mailto:hello@atmybest.com" target="_blank"><img id="mail-icon"
                                                                                              src="{{ asset("img/email.png") }}"
                                                                                              alt=""/> hello@atmybest
                                .com </a>
                            <a class="col-sm-6 col-xs-12" href="https://twitter.com/At_my_best" target="_blank"> <img id="twitter-icon"
                                                                                                    src="{{ asset("img/twitter.png") }}"
                                                                                                    alt=""/> At my best</a>
                        </p>

                    </div>
                </div>
            </div>

        </div>

        <div class="slide" data-slidr="two">

            <h2>Your strengths <span class="orange">reflected</span></h2>

            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <a id="photo" class="info collapsed" data-toggle="collapse" data-target="#photoresult" aria-expanded="false"
                       aria-controls="photoresult" onclick="openInfo('#photo','#photoresult');">
                        Top tips
                    </a>

                    <div class="row top15"></div>
                    @include("info.results.photo")
                </div>
            </div>

            <!-- Chosen image and notes display -->
            <div class="row">
                <div class="container">
                    <div id="result-image" class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <img id="{{ $result->image_id }}" src="{{ asset("img/cards/" . $result->image_id) }}.jpg"
                                 alt="{{ Card::find($result->image_id)->title }}" class="img-responsive"/>
                        </div>
                        <div class="col-sm-2"></div>

                    </div>

                    <div id="result-note" class="row">
                        <div class="col-sm-2"></div>

                        <div class="col-sm-8">
                            <p>{{$result->image_note}}</p>
                        </div>
                        <div class="col-sm-2"></div>

                    </div>

                </div>
            </div>

        </div>


        <div class="slide" data-slidr="three">

            <h2>Your strengths <span class="orange">reflected</span></h2>

            <p>Strengths across contexts</p>

            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <a id="veninfo" class="info collapsed" data-toggle="collapse" data-target="#venndiagram" aria-expanded="false"
                       aria-controls="venndiagram" onclick="openInfo('#veninfo','#venndiagram');" >
                        Top tips
                    </a>

                    <div class="row top15"></div>
                    @include("info.results.venn")
                </div>
            </div>

            <div id="venn" class="venn"></div>

        </div>

        <div class="slide" data-slidr="four">

            <h2>Your strengths <span class="orange">reflected</span></h2>

            <p>Which strengths did you choose?</p>

            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <a id="card" class="info collapsed" data-toggle="collapse" data-target="#cards" aria-expanded="false"
                       aria-controls="cards" onclick="openInfo('#card','#cards');">
                        Top tips
                    </a>

                    <div class="row top15"></div>
                    @include("info.results.cards")
                </div>
            </div>

            <div class="container">
                <div class="row" id="result-stacked-cards">

                    @foreach($contexts as $index => $context)
                        <div class="col-sm-3 col-sm-offset-2">
                            <p class="impact-title">{{ $index }}</p>
                            @foreach($context as $choice)
                                <?php $card = Card::find($choice); ?>
                                <section class="card result">
                                    <label for="{{  "card" . $card->id }}">
                                        <img src="{{ asset("img/logo.png") }}" alt="{{ $card->title }}"/>

                                        <p>{{ $card->title }}</p>
                                    </label>
                                </section>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>

        </div>

        <div class="slide" data-slidr="five">

            <h2>You at <span class="orange">your best</span>...</h2>

            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <a id="cloudinfo" class="info collapsed" data-toggle="collapse" data-target="#wordcloud" aria-expanded="false"
                       aria-controls="wordcloud" onclick="openInfo('#cloudinfo','#wordcloud');">
                        Top tips
                    </a>

                    <div class="row top15"></div>
                    @include("info.results.wordcloud")
                </div>
            </div>

            <div class="cloud-holder">
                <img class="background" src="{{ asset("/img/cards/" . $result->image->image) }}" />
                <div id="cloud" class="cloud"></div>
                <div class="clear"></div>
            </div>

        </div>


    </div>

@stop

@section("scripts")
    <script>
        var data = {{ $data }};

        function openInfo(info, target) {

            var info = $(info);
            var opened = info.attr('aria-expanded');
            var original = $('.krakatoa-container').height();
            if (opened == 'false') {
                var extended;
                setTimeout(function () {
                    extended = $(target).height();
                    var newheight = String(extended + original) + 'px';
                    $('.krakatoa-container').css('height', newheight);
                    info.click(function () {
                        $('.krakatoa-container').css('height', original);
                    });
                    $('.close-info').click(function () {
                        $('.krakatoa-container').css('height', original);
                    })
                }, 300);


            }

        }


    </script>
    <script src="{{ asset('js/vendor/jquery.krakatoa.js')}}"></script>
    <script src="{{ asset('js/d3.js')}}"></script>
    <script src="{{ asset('js/Venn.js') }}"></script>
    <script src="{{ asset('js/wordCloud.js')}}"></script>
    <script src="{{ asset('js/d3.layout.cloud.js')}}"></script>
    <script src="{{ asset('js/results.js')}}"></script>

@stop