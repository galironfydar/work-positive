@extends("templates.master")

@section("header")

    @include("templates.header")

@stop

@section("content")

    <div class="container error-page">

        <h1 class="orange">404 - Page not found</h1>
        <p>Something went wrong, please return to the <a href="{{ URL::to('/') }}">index page</a></p>

    </div>

@stop
@section("scripts")

@stop