@extends("templates.master")

@section("header")

    <div class="container home-page">

        {{--<img class="logo" src="img/logo_white.png" alt="Solo"/>--}}
        {{--<h1>Solo</h1>--}}

        <img class="logo-header" src="{{ asset("img/Solo_White_only.png") }}" alt="Solo"/>

        <p class="lead">A fun and interactive way to identify<br>and reflect on your strengths</p>

        <div class="row">

            <div class="col-sm-4 col-sm-offset-2">
                <a class="info collapsed info-header" data-toggle="collapse" href="#benefits" aria-expanded="false"
                   aria-controls="benefits">
                    What is reflector?
                </a>

                <div class="row top15"></div>

            </div>

            <div class="col-sm-4">
                <a class="info collapsed info-header" data-toggle="collapse" href="#time" aria-expanded="false"
                   aria-controls="time">
                    How long does it take?
                </a>

                <div class="row top15"></div>

            </div>

            <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">

                @include("info.index.benefits")

                @include("info.index.time")

            </div>

        </div>

    </div>

@stop

@section("content")

    <div class="container index">

        {{ Form::open() }}

        <h2>Which <span class="orange">two parts</span> of your life would you like to focus on today?</h2>

        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-8 col-lg-offset-2">
                <a class="info collapsed info-header-index" data-toggle="collapse" href="#comparing" aria-expanded="false"
                   aria-controls="comparing">
                    How will comparing two contexts help me?
                </a>

                <div class="row top15"></div>

                @include("info.index.comparing")

            </div>
        </div>

        <!-- Contexts Display -->
        <div class="row card-row">
            <div class="container">
                <div class="col-lg-12 col-lg-offset-2">
                    @foreach($contexts as $context)
                        @include("partials.context")
                    @endforeach
                </div>
            </div>
        </div>

    </div>

    <br/>
    <br/>
    <br/>



    <!-- Navigation -->

    <section class="fixed-elements">

    <div id="sticky-nav" class="row">

        <div class="container">

            <div class="col-sm-10">
                <div class="progress-holder">
                    <span class="circle active">1</span>
                    <span class="circle">2</span>
                    <span class="circle">3</span>
                    <span class="circle">4</span>
                    <span class="circle">5</span>
                    <span class="circle">6</span>
                    <span class="circle">7</span>
                </div>
            </div>

            <div class="col-sm-2">
                {{ Form::submit('Next', ['class' => 'next-btn btn btn-block btn-orange', 'disabled' => true]) }}
            </div>

        </div>

    </div>
     </section>

    {{ Form::close() }}

@stop

@section("scripts")

    <script>
        var min = 2,
                max = 2;
    </script>
    <script src="{{ asset("js/card-selection.js") }}"></script>

@stop