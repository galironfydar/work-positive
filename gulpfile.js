var gulp = require("gulp"),
    sass = require("gulp-sass"),
    jshint = require("gulp-jshint"),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    livereload = require("gulp-livereload"),
    // Files
    sassFiles = [
        "./public/sass/style.scss",
        "./public/sass/blocss/blocss.scss",
    ],
    jsFiles = [
        "./public/js/**/*.js"
    ];

/**
 * Workflow
 */

// SASS Compile
gulp.task("compile-sass", function () {
    gulp.src(sassFiles)
        .pipe(sass({ errLogToConsole: true }))
        .pipe(concat("style.css"))
        .pipe(gulp.dest("public/css"))
        .pipe(livereload());
});

// Lint the "app" folder containing all the angular files
gulp.task("lint-app", function () {
    gulp.src(jsFiles)
        .pipe(jshint())
        .pipe(jshint.reporter("default"));
});

// Bundle and uglify all the angular js files and output the minified version
gulp.task("js-bundle", function () {
    gulp.src(jsFiles)
        .pipe(concat("dist.js"))
        .pipe(uglify())
        .pipe(gulp.dest("./public/js/"));
});

gulp.task("watch", function () {
    livereload.listen();
    gulp.watch(jsFiles);
    gulp.watch("./public/sass/**/*.scss", ["compile-sass"]);
    //gulp.watch("./public/js/**/**.js", ["js-bundle"]);
});

gulp.task("dev", ["compile-sass", "watch"]);
