var min = 5,
    max = 12,
    next = $('.l-footer').find('.next-btn');

$(document).ready(function() {

    $('.card').find('input').on('click', function(event) {

        var selected = numSelected();

        if(selected > max) {
            event.preventDefault();
            return false;
        }

        if(selected >= min && selected <= max)
            next.prop('disabled', false);
        else
            next.prop('disabled', true);

        updateSelected(selected);

    });

});

var numSelected = function() {
    return $('input[type=checkbox]:checked').length;
};

var updateSelected = function(selected) {
    var num = $('.footer-stats').find('.num');
    num.text(selected);
};