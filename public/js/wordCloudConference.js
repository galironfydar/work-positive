function drawWordConfCloud(results) {

    if(results.length > 0) {

        var words = [],
            max = results[0].count;

        console.log(results.length);

        for(var i = 0; i < results.length; i++) {

            if(screen.width < 1600) {
                var weight = (60/max) * results[i].count;
            } else {
                var weight = (100/max) * results[i].count;
            }

            words.push({ title: results[i].title, weight: (weight < 4) ? 4 : weight  });

        }

        var container = $( ".cloud-holder" );
        var h = container.height();
        var w = container.width();
        var h2 = h/2;
        var w2 = w/2;

        console.log(words);

        var fill = d3.scale.category20();
        d3.layout.cloud().size([w, h])
            .startPoint([w / 2.0, h / 2.0])
            .words(words.map(function (d) {
                return {text: d.title, size: d.weight};
            }))
            .fontSize(function (d) {
                return d.size;
            })
            .padding(20)
            .timeInterval(1)
            .rotate(0)
            .on("end", draw)
            .start();

        function draw(words) {
            d3.select("#cloud")
                .append("div")
                .classed("svg-container", true)
                .append("svg")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .attr("viewBox", "0 0 " + w + ' ' + h)
                .classed("svg-content-responsive", true)
                .append("g")
                .attr("transform", "translate(" + w2 + "," + h2 + ")")
                .selectAll("text")
                .data(words)
                .enter()
                .append("text")
                .transition()
                .duration(500)
                .style("font-size", function (d) {
                    return d.size + "px";
                })
                .style("font-family", "museo-sans-rounded")
                .style("fill", "white")
                .attr("text-anchor", "middle")
                .attr("transform", function (d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .text(function (d) {
                    return d.text;
                });
        }

    }

}

function reDrawConfWordCloud(data) {
    document.getElementById("cloud").innerHTML = "";
    drawWordConfCloud(data);
}
console.log(window.innerWidth);
