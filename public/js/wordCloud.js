function drawWordCloud(words) {
    $('.cloud').html("");
    console.log(words);
    var allWords = [];
    var data = words;
    var obj = {};
    var duplicatedWords = [];
    $('.cloud').css('height', $('.background').height() + 'px');
    $('.cloud').css('width', $('.background').width() + 'px');
    var container = $(".cloud");
    var h = container.height();
    var w = container.width();
    var h2 = h / 2;
    var w2 = w / 2;
    for (var i = 0; i < data.contexts.length; i++) {
        for (var key in data.contexts[i].strengths) {
            if (data.contexts[i].strengths.hasOwnProperty(key)) {
                for (var d = 0; d < data.contexts[i].strengths[key].length; d++) {
                    obj = {};
                    var high = 0;
                    var med = 0;
                    var low = 0;
                    obj.importance = key;
                    obj.text = data.contexts[i].strengths[key][d];
                    switch (key) {
                        case "high":
                            obj.weight = 36 - (high);
                            high++;
                            break;
                        case "medium":
                            obj.weight = 24 - (med);
                            med++;
                            break;
                        case "low":
                            obj.weight = 12 - (low);
                            low++;
                            break;
                        default:
                            obj.weight = 0;
                            break;
                    }
                    allWords[allWords.length] = obj;
                }
            }
        }

    }

    for (var i = 0; i < allWords.length; i++) {
        for (var j = 0; j < allWords.length; j++) {
            if (allWords[i].text === allWords[j].text && !(i === j)) {
                duplicatedWords[duplicatedWords.length] = allWords[i];
                (allWords[i].weight += allWords[j].weight) * 0.75;
                allWords.splice(j, 1);
            }
        }
    }
    allWords.sort(function (a, b) {
        return parseFloat(b.weight) - parseFloat(a.weight)
    });


    function colour() {

        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }

        return color;
    }


    var scale = 1.5;

    if (w <= 800) {
        scale = 0.6;
    } else if (w <= 1100) {
        scale = 1;
    }

    var fill = d3.scale.category20();

    d3.layout.cloud().size([w, h])
        .startPoint([w / 2, h / 2])
        .words(allWords.map(function (d) {
            return {text: d.text, size: d.weight * scale};
        }))
        .fontSize(function (d) {
            return d.size;
        })
        .padding(15)
        .timeInterval(1)
        .rotate(0)
        .on("end", draw)
        .start();

    function draw(words) {
        d3.select("#cloud")
            .append("div")
            .classed("svg-container", true)
            .append("svg")

            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", "0 0 " + w + ' ' + h)
            .classed("svg-content-responsive", true)
            .append("g")
            .attr("transform", "translate(" + w2 + "," + h2 + ")")
            .selectAll("text")
            .data(words)
            .enter()
            .append("text")
            .transition()
            .duration(500)
            .style("font-size", function (d) {
                return d.size + "px";
            })
            .style("font-family", "museo-sans-rounded")
            .style("fill", "black")
            .attr("text-anchor", "middle")
            .attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function (d) {
                return d.text;
            });
    }
}

function reDrawWordCloud(data) {
    document.getElementById("cloud").innerHTML = "";
    drawWordCloud(data);
}

