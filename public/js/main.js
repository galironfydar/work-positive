'use strict';

//used for the back button in firefox
window.onunload = function () {
};

$(window).ready(function () {

    //setTimeout(function() {
    //    $('.preloader').fadeOut();
    //}, 1000);
    //
    //$('.next-btn').click(function() {
    //    $('.preloader').fadeIn();
    //});

    $('.info').on('click', function () {
        $('body').find('.collapse.in').collapse('hide');
    });

    //Add to exclude

    var resultPath = window.location.pathname;
    var resultPath = resultPath.indexOf("/results") > -1;


    if (window.location.pathname != '/questionnaire'
        && resultPath != true && window.location.pathname != '/conference'
        && window.location.pathname != '/thanks') {

        //Fixes Elements
        $('.fixed-elements').scrollToFixed({
            bottom: 0,
            limit: $('.fixed-elements').offset().top

        });

        //if page is at bottom add overflow hidden else don't. Fix for sort page when dragging cards
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {

                $('.fixed-elements').css({
                    'overflow': 'hidden',
                });

            } else {

                $('.fixed-elements').css({
                    'overflow': 'initial',
                });

            }
        });


    }

});

function goBack() {
    history.back(-1);
}

$.fn.serializeObject = function () {
    var o = [];
    var a = this.serializeArray();
    $.each(a, function () {
        o.push(this.value);
    });
    return o;
};