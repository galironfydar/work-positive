var next = $('.next-btn');
var cardID = 0;

$(document).ready(function ($) {

    //Set Defaults
    updateNextButton();

    // delegate calls to data-toggle="lightbox"
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();

        return $(this).ekkoLightbox({
            always_show_close: true,
            width:940
        });
    });

    $(document.body).on("click", ".pick", function(event) {

        $('.preloader').fadeIn();

       //Remove Currently Selected Card (If Any)
        $('#'+cardID).removeClass('selected');

        //Get New ID of the Card
        cardID = event.target.id;

        $('input[name=id]').val(cardID);

        $('.image-form').submit();

    });
});

var updateNextButton = function() {
    if(cardID > 0 ) {
        next.prop('disabled', false);
    } else {
        next.prop('disabled', true);
    }
}
