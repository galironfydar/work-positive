var next = $('.next-btn'),
    count = $('.count');

$(document).ready(function() {

    var selected = numSelected();
    updateSelected(selected);

    $('.card').find('input').on('click', function(event) {

        var selected = numSelected();

        if(selected > max) {
            event.preventDefault();
            return false;
        }

        updateSelected(selected);

    });

});

var numSelected = function() {
    return $('input[type=checkbox]:checked').length;
};

var updateSelected = function(selected) {
    if(selected >= min && selected <= max) {
        next.prop('disabled', false);
    } else {
        next.prop('disabled', true);
    }
    count.text(selected);
};