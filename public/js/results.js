$(document).ready(function () {

    var slider;
    diagram = new Venn.create(data, 'venn');
    drawWordCloud(data);
    slider = $('.results-slider').krakatoa({
        width: '100%',
        height: 'auto',
        autoplay: false,
        buttons: false,
        loop: true
    });

    var resizeTimer;

    $(window).on('resize', function (e) {

        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {

            drawWordCloud(data);

        }, 250);

    });


    $('#venndiagram').collapse({
        show: true
    });

    $('#wordcloud').collapse({
        show: true
    });

    $('#cards').collapse({
        show: true
    });

    $('#photoresult').collapse({
        show: true
    });

});