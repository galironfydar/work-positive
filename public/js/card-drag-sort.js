var next = $('.next-btn');

$(document).ready(function () {

    if(cardsSorted == 1) {
        next.prop('disabled', false)
    }

    $('.sortable.cards').sortable({
        connectWith: '.impact .droppable',
        cursor: 'move',
        stop: function(event, ui) {
            if($(this).find('.card').length == 0)
                $(this).parents('.shortlist').remove();
        }
    });

    // Init sortable on impacts
    $('.impact .droppable').sortable({
        connectWith: '.impact .droppable',
        cursor: 'move',
        update: function() {
            if(num_cards == $('.droppable .card').length)
                next.prop('disabled', false)
        },
        beforeStop: function(ev, ui) {
            if (!$(ui.item).hasClass('card')) {
                $(this).sortable('cancel');
            }
        }
    });

    $('.next-btn').on('click', submitResults);

});

var submitResults = function() {

    var cards = {
        "low": $('#low').serializeObject(),
        "med": $('#med').serializeObject(),
        "high": $('#high').serializeObject()
    };

    $.ajax({
        type: "POST",
        url: BASE_URL + "/" + context + "/sort",
        data: {
            strengths: cards,
            context: $('input[name=context]').val()
        },
        success: function(response) {
            if(response.status == 'success')
                window.location.href = response.url;
        }
    });

};

$.fn.serializeObject = function()
{
    var o = [];
    var a = this.serializeArray();
    $.each(a, function() {
        o.push(this.value);
    });
    return o;
};