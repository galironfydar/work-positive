var next = $('.next-btn');
next.prop('disabled', false);

$(document).ready(function () {

    $('.questionnaire-form').on('submit', function() {
        $('.preloader').fadeIn();
    });

    // Get Json Data
    var result = $.getJSON("/js/cbyc.json", function (json) {});
    result.done(function (json) {

        console.log(1);
        // Populate Continents Select BOx
        var continents = ["Africa", "Antarctica", "Asia", "Europe", "North America", "Oceania", "South America"];

        $.each(continents, function (key, value) {
            $('#continentlist')
                .append($("<option></option>")
                    .attr("value", value)
                    .text(value));
        });


        //On Change Of Continents
        $("#continentlist").change(function () {

            setDefaultCountriesList();
            getCountryList(this.value);

            function getCountryList(country) {

                var countryList = [];

                for (i = 0; i < json["countries"]["country"]["length"]; i++) {
                    if (json["countries"]["country"][i]["continentName"] == country) {
                        countryList.push(json["countries"]["country"][i]["countryName"]);
                    }
                }

                countryList.sort();

                $.each(countryList, function (key, value) {
                    $('#countrylist')
                        .append($("<option></option>")
                            .attr("value", value)
                            .text(value));
                });

            }

        }); // End Change
    }); // End Promise

    /* Sliders */
    $("#slider1").slider({
        value: 5,
        min: 0,
        max: 10,
        step: 1,
        animate: true,
        slide: function (event, ui) {
            $("#slider1amount").val(ui.value);
        }
    });
    $("#slider1amount").val($("#slider1").slider("value"));


    $("#slider2").slider({
        value: 5,
        min: 0,
        max: 10,
        step: 1,
        animate: true,
        slide: function (event, ui) {
            $("#slider2amount").val(ui.value);
        }
    });
    $("#slider2amount").val($("#slider2").slider("value"));


});


//Set Empty Countries List
function setDefaultCountriesList() {

    $('#countrylist')
        .find('option')
        .remove()
        .end()
        .append('<option value="default" selected="selected">Please select your country</option>')
    ;

}
