// Get Json Data
var result = $.getJSON("/js/cbyc.json", function (json) {
});
result.done(function (json) {

    // Populate Continents Select BOx
    var continents = ["Africa", "Antarctica", "Asia", "Europe", "North America", "Oceania", "South America"];

    $.each(continents, function (key, value) {
        $('#continentlist')
            .append($("<option></option>")
                .attr("value", value)
                .text(value));
    });


    //On Change Of Continents
    $("#continentlist").change(function () {

        setDefaultCountriesList();
        getCountryList(this.value);

        function getCountryList(country) {

            var countryList = [];

            for (i = 0; i < json["countries"]["country"]["length"]; i++) {
                if (json["countries"]["country"][i]["continentName"] == country) {
                    countryList.push(json["countries"]["country"][i]["countryName"]);
                }
            }

            countryList.sort();

            $.each(countryList, function (key, value) {
                $('#countrylist')
                    .append($("<option></option>")
                        .attr("value", value)
                        .text(value));
            });

        }

    }); // End Change
}); // End Promise

function setDefaultCountriesList() {

    $('#countrylist')
        .find('option')
        .remove()
        .end()
        .append('<option value="" selected="selected">Please select your country</option>');

}

$(document).ready(function () {

    setInterval(function () {
        console.log('hit');
        loadData();
    }, 120000);


});

function loadData() {

    var filters = {
        'continent': $('select[name=continent]').val(),
        'country': $('select[name=country]').val(),
        'gender': $('input[name=gender]:checked').val(),
        'interests': $('form.interest-form').serializeObject()
    };

    $.ajax({
        type: "POST",
        url: "/conference",
        data: {
            filters: filters
        },
        success: function (data) {
            reDrawConfWordCloud(data.results);
            updateNum(data.users);
        }
    });

}

function updateNum(numOfUsers) {
    console.log(numOfUsers);
    var tag = document.getElementById("users");
    if (numOfUsers > 0) {
        tag.innerHTML = (numOfUsers);
    } else {
        tag.innerHTML = (0);
    }
}
