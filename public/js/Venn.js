/**
 * Created by Christopher on 29/04/2015.
 */

// Venn Diagram 2: 'The Refactoring'...

var Venn = (function () {

    // Diagram Object
    function Diagram() {

        this.properties = {
            width: null,
            height: null,
            padding: 0,
            dataset: null
        };

        this.chart = {
            container: null,
            groupdata: [],
            groupcontainer: null, // delete
            color: {
                orange: ['rgb(243, 147, 33)', 'rgb(244, 165, 86)'],
                grey: ['rgb(205, 201, 195)', 'rgb(110, 111, 113)']
            },
            duplicated: null
        };

        this.data = function (dataset) {
            this.properties.dataset = dataset;
            return this;
        };

        this.create = function (element) {
            var e = $('#' + element);

            this.properties.width = e.width();
            this.properties.height = e.height();

            // Create the svg using D3
            this.chart.container = d3.select('#' + element)
                .append("div")
                .classed("svg-container", true)
                .classed("venn-container", true)
                .append("svg")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .attr("viewBox", "0 0 " + this.properties.width + ' ' + this.properties.height)
                .classed("svg-content-responsive", true)
                .append('g')
                .attr('transform', 'translate(2,2)');

            return this;
        };

        this.group = function () {

            // We now have the svg to work with so we need to work out what
            // data we will be working with, and what we are drawing.

            var group = 0;
            var chart = this.chart.color;

            // draw circles based on a group.
            this.chart.groupcontainer = this.chart.container.selectAll('g')
                .data(this.chart.groupdata)
                .enter()
                .append('g')
                .attr('class', 'group')
                .append('g')
                .each(function (d) {
                    var intersect = true;
                    var color = [];

                    // Define the gradient


                    if (d.name !== 'intersect') {
                        group += 1;
                        var multiplier = d.r;

                        if (!intersect) multiplier = d.r * 2;
                        if (group === 1) color = chart.grey;
                        if (group === 2) color = chart.orange;

                        var gradient = d3.select(this)
                            .append("svg:defs")
                            .append("svg:linearGradient")
                            .attr("id", group)
                            .attr("x1", "0%")
                            .attr("y1", "0%")
                            .attr("x2", "100%")
                            .attr("y2", "100%")
                            .attr("spreadMethod", "pad");

                        // Define the gradient colors
                        gradient.append("svg:stop")
                            .attr("offset", "0%")
                            .attr("stop-color", color[0])
                            .attr("stop-opacity", 1);

                        gradient.append("svg:stop")
                            .attr("offset", "100%")
                            .attr("stop-color", color[1])
                            .attr("stop-opacity", 1);

                        d3.select(this)
                            .attr('class', 'group-g-' + group)
                            .append('circle')
                            .attr('cx', function (d) {
                                return d.x;
                            })
                            .attr('cx', function (d) {
                                if (group === 1) return d.x + (multiplier / 2);

                                if (group === 2) return d.x - (multiplier / 2);

                                return d.x;
                            })
                            .attr('cy', function (d) {
                                return d.y;
                            })
                            .attr('r', function (d) {
                                return d.r;
                            })
                            .attr('class', 'group-c-' + group)
                            .style('stroke', 'none')
                            .attr('fill-opacity', 0.7)
                            .attr('fill', 'url(#' + group + ')');

                        d3.select(this)
                            .append('circle')
                            .attr('cx', function (d) {
                                if (group === 1) return d.r * Math.cos(230 * (Math.PI / 180)) + d.x - (multiplier / 2) - 70;

                                if (group === 2) return d.r * Math.cos(50 * (Math.PI / 180)) + d.x + (multiplier / 2) + 70;

                            })
                            .attr('cy', function (d) {
                                return d.r * Math.sin(220 * (Math.PI / 180)) + d.y - 70;
                            })
                            .attr('r', function (d) {
                                return 60;
                            })
                            .attr('fill', 'none')
                            .attr('stroke', 'rgb(234,234,234)');

                        d3.select(this)
                            .append('text')
                            .attr('x', function (d) {
                                if (group === 1) return d.r * Math.cos(230 * (Math.PI / 180)) + d.x - (multiplier / 2) - 70;

                                if (group === 2) return d.r * Math.cos(50 * (Math.PI / 180)) + d.x + (multiplier / 2) + 70;
                            })
                            .attr('y', function (d) {
                                return d.r * Math.sin(220 * (Math.PI / 180)) + d.y - 65;
                            })
                            .attr("class", "text-title")
                            .attr("text-anchor", "middle")
                            .style('font-size', '15px')
                            .text(function (d) {
                                return d.name;
                            });


                    }
                });

            return this;
        };

        this.child = function () {
            // draw children within the rect.

            var element = 0;

            this.chart.container = d3.selectAll('.group')
                .each(function (d) {

                    element += 1;

                    for (var child = 0; child < d.children.length; child++) {
                        var text = d.children[child].name;

                        d3.select(this).append('text')
                            .attr('x', function (d) {
                                if (element === 1) return d.r * Math.cos(((120 / d.children.length) * child + 315) * (Math.PI / 180)) + d.x;

                                if (element === 2) return d.r * Math.cos(((120 / d.children.length) * child - 235) * (Math.PI / 180)) + d.x;

                                if (d.name === 'intersect') return d.x;

                            })
                            .attr('y', function (d) {
                                if (element === 1) return d.r * Math.sin(((120 / d.children.length) * child + 315) * (Math.PI / 180)) + d.y;

                                if (element === 2) return d.r * Math.sin(((120 / d.children.length) * child - 235) * (Math.PI / 180)) + d.y;

                                if (d.name === 'intersect') {


                                    if (d.children.length >= 4) {
                                        return d.y - 135 + (child * 25);
                                    }

                                    if (d.children.length < 4) {
                                        return d.y - 50 + (child * 30);
                                    }
                                }

                            })
                            .text(function (d) {
                                return d.children[child].name;
                            })
                            .attr("text-anchor", "middle")
                            .attr('class', 'text');
                    }
                });

            return this;
        };

    }

    /************************* UTIL METHODS ****************************/

        // Name the contexts'
    Diagram.prototype.eachContext = function () {
        var data = this.properties.dataset;
        var compare;
        var contexts = [];

        for (var i = 0; i < data.contexts.length; i++) {
            var r = 0;
            var duplicated = [];
            var children;
            contexts.push(data.contexts[i].name);

            r = (this.properties.width / data.contexts.length ) / 2;

            if (children !== undefined && children !== null) {
                compare = children;
            }

            // recurse children into a flat array.
            children = this.sortChildren(data.contexts[i]);

            if (compare !== undefined && compare !== null) {


                for (var key in compare) {
                    var word = compare[key].name;
                    for (var k in children) {
                        if (children[k].name === word) {
                            duplicated.push({'name': children[k].name, 'weight': 10});
                        }
                    }
                }

            }


        }


        for (var dup in duplicated) {
            for (var key in compare) {
                if (duplicated[dup].name === compare[key].name) {
                    if (compare.hasOwnProperty(key)) {
                        compare.splice(key, 1);
                    }
                }
            }
        }

        for (var dup in duplicated) {
            for (var key in children) {
                if (duplicated[dup].name === children[key].name) {

                    if (children.hasOwnProperty(key)) {
                        children.splice(key, 1);
                    }
                }
            }
        }

        this.chart.groupdata.push({
            'name': contexts[0],
            'x': this.properties.width / 2,
            'y': this.properties.height / 2,
            'r': r,
            'children': children
        });
        this.chart.groupdata.push({
            'name': contexts[1],
            'x': this.properties.width / 2,
            'y': this.properties.height / 2,
            'r': r,
            'children': compare
        });
        this.chart.groupdata.push({
            'name': 'intersect',
            'x': this.properties.width / 2,
            'y': this.properties.height / 2,
            'r': r,
            'children': duplicated
        });

        return this;


    };

    Diagram.prototype.sortChildren = function (data) {
        var arr = [];

        for (var key in data.strengths) {
            var weight = 0;
            if (key === 'high') weight = 10;
            if (key === 'medium') weight = 9;
            if (key === 'low') weight = 8;

            var compare = 'comp';

            for (var i = 0; i < data.strengths[key].length; i++) {


                compare = data.strengths[key][i];

                arr.push({'name': data.strengths[key][i], 'weight': weight});

            }

        }

        return arr;
    };


    // our object of methods.
    this.methods = {
        diagram: function () {
            return new Diagram();
        },
        // To be able to create a diagram we want the element of the div, plus the dataset.
        create: function (dataset, element) {
            this.diagram = new Diagram()
                .data(dataset)
                .create(element)
                .eachContext()
                .group()
                .child();

            return this.diagram;
        }
    };

    return methods;
}());

